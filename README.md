# [Sublime Text 2](http://www.sublimetext.com/2) snippets and prefs by @[jonginn](http://twitter.com/jonginn)

This is mostly for my personal use, although I've made it public in the hope that it makes someone else's life a bit easier. Here's a collection of my snippets I use in Sublime Text 2, and I'll be adding to these over time.

I'll add user guides in time if people want them.

## Installation

**Snippets:** Drag the snippet folders (HTML, CSS, etc) into your users folder. (See below)  
**Keyprefs:** It'll probably be simplest to merge your existing keyprefs manually or simply view mine and approve/disapprove of my choices.

###Windows
(You will need to enable hidden folders)

	C:\Users\YOUR USERNAME HERE\AppData\Roaming\Sublime Text 2\Packages\User

###Mac OSX
	~/Library/Application Support/Sublime Text 2/Packages/User/


# Authors
**Jonathan Ginn**
+ http://twitter.com/jonginn
+ http://github.com/jonginn

*Ian Davenport* for the repeat class.
+ http://twitter.com/iangeek